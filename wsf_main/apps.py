from django.apps import AppConfig


class WsfMainConfig(AppConfig):
    name = 'wsf_main'
